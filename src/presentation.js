import React from 'react';
// Import React

// Import Spectacle Core tags
import {
  BlockQuote,
  Image,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  Magic,
  Appear,
  ComponentPlayground,
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

// Require CSS
require('normalize.css');

const cubosBlue = "#31a2dd";
const cubosPink = "#ff00ff";
const brutalistYellow = "#eaff00";

const theme = createTheme(
  {
    primary: cubosBlue,
    secondary: brutalistYellow,
    tertiary: "white",
    quartenary: cubosPink,
    cubosBlue,
    cubosPink,
    brutalistYellow,
  },
  {
    primary: 'Zilla Slab',
    secondary: 'Helvetica',
  }
);

const editorBackgroundColor = "#2a2734";

const helloWorldExample = `
function HelloWorld(props) {
  return <h1>Olá, {props.name}!</h1>;
}

function WhatThe() {
  return <h2>HTML no Javascript???</h2>;
}

function App() {
  return (
    <main>
      <HelloWorld name="React"/>
      <WhatThe/>
    </main>
  );
};

render(<App/>);
`;



const jsxExample = `
const Student = ({ name, grade }) => (
  <tr>
    <th>{name}</th>
    <td>{grade}</td>
    {grade >= 5 && <td>Parabéns!</td>}
  </tr>
);

const students = [
  { name: "Joselito", grade: 3 },
  { name: "Josefina", grade: 8 },
  { name: "Josualdo", grade: 5 },
];

const App = () => (
  <main>
    <h1>Estudantes</h1>
    <table style={{ margin: "auto" }}>
      <thead>
        <th>Nome</th>
        <th>Nota</th>
        <th>Observações</th>
      </thead>
      <tbody>{
        students.map(stud => (
          <Student
            name={stud.name}
            grade={stud.grade}
          />
        ))
      }</tbody>
    </table>
  </main>
);

render(<App/>);
`;

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['slide']}
        transitionDuration={500}
        theme={theme}
        progress="bar"
        contentWidth={1200}
      >
        <Slide transition={['zoom']} progressColor="quartenary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            React
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            componentes: quem são e onde habitam
          </Text>
        </Slide>
        <Slide bgColor="tertiary" progressColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="quartenary">
            Quem
          </Heading>
          <Heading size={2} fit caps lineHeight={1} textColor="primary">
            Victor Magalhães
          </Heading>
          <Text margin="10px 0 0" textColor="quartenary" size={1} fit bold>
            desenvolvedor javascript em <span style={{ color: cubosBlue }}>cubos.io</span>
          </Text>
        </Slide>
        <Magic progressColor="quartenary">
          <Slide bgColor="tertiary">
            <Heading size={2} caps lineHeight={1} textColor="primary">
              Quem já desenvolveu pra web?
            </Heading>
          </Slide>
          <Slide bgColor="tertiary">
            <Heading size={2} caps lineHeight={1} textColor="primary">
              Quem já desenvolveu em js?
            </Heading>
          </Slide>
        </Magic>
        <Magic progressColor="quartenary">
          <Slide bgColor="tertiary">
            <Heading size={2} textColor="quartenary">I</Heading>
            <Heading size={2} textColor="quartenary">C</Heading>
            <Heading size={2} textColor="quartenary">Y</Heading>
            <Heading size={2} textColor="quartenary">M</Heading>
            <Heading size={2} textColor="quartenary">I</Heading>
          </Slide>
          <Slide bgColor="tertiary">
            <Heading size={2} textColor="quartenary">In</Heading>
            <Heading size={2} textColor="quartenary">Case</Heading>
            <Heading size={2} textColor="quartenary">You</Heading>
            <Heading size={2} textColor="quartenary">Missed</Heading>
            <Heading size={2} textColor="quartenary">It</Heading>
          </Slide>
        </Magic>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              A web evoluiu
            </Heading>
            <Image src="https://media.giphy.com/media/8fCGJdconX6IE/giphy.gif" width={400}/>
        </Slide>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              De uma web de documentos
            </Heading>
            <Image src="https://media.giphy.com/media/pnzDnYiIYaky4/giphy.gif" height={400}/>
        </Slide>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              Para uma web de aplicativos
            </Heading>
            <Image src="https://media.giphy.com/media/dIBTQmeIc30AWsbgRr/source.gif" width={400}/>
        </Slide>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              O mesmo vale pra suas ferramentas
            </Heading>
            <Image src="https://media.giphy.com/media/3oKIPqsXYcdjcBcXL2/source.gif" width={400}/>
        </Slide>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              HTML
            </Heading>
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              CSS
            </Heading>
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              Javascript
            </Heading>
        </Slide>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              De JQuery
            </Heading>
            <Image src="https://media.giphy.com/media/xThuWu82QD3pj4wvEQ/giphy.gif" width={400}/>
        </Slide>
        <Slide bgColor="quartenary">
            <Heading size={2} caps lineHeight={1} textColor="tertiary">
              Para
            </Heading>
            <Image src="https://media.giphy.com/media/8myUSdiP0XZFPL1R2b/giphy.gif" width={400}/>
        </Slide>
        <Slide bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            React?
          </Heading>
          <div style={{ display: "grid", gridAutoFlow: "column", gridGap: "8pt" }}>
            <List textColor="tertiary">
              <ListItem>Biblioteca javascript</ListItem>
              <ListItem>Interfaces de usuário</ListItem>
              <ListItem>Componentes</ListItem>
            </List>
            <List textColor="tertiary">
              <ListItem>JSX</ListItem>
              <ListItem>DOM virtual</ListItem>
              <ListItem>Declarativa</ListItem>
            </List>
          </div>
        </Slide>
        <Slide bgColor="tertiary" progressColor="quartenary">
          <Heading size={1} caps lineHeight={1} textColor="primary">
            React:
          </Heading>
          <ComponentPlayground
            code={helloWorldExample}
            previewBackgroundColor={editorBackgroundColor}
          />
        </Slide>
        <Slide
          bgColor="tertiary"
          progressColor="quartenary"
          notes="condicional, map, propriedades e interpolação"
        >
          <Heading size={1} caps lineHeight={1} textColor="primary">
            JSX!
          </Heading>
          <Text margin="10px 0" textColor="primary" size={2}>
            <b>j</b>ava<b>s</b>cript e<b>x</b>tension
          </Text>
          <ComponentPlayground
            code={jsxExample}
            previewBackgroundColor={editorBackgroundColor}
          />
        </Slide>
        <Slide bgColor="tertiary" progressColor="quartenary">
          <Heading size={1} caps lineHeight={1} textColor="primary">
            UI como <em>função</em> do <em>estado</em>
          </Heading>
          <Text margin="10px 0" textColor="primary" size={2}>
            Você <b>declara</b> como a UI deve ser e o React faz as mudanças necessárias para você
          </Text>
        </Slide>
        <Slide bgColor="tertiary" progressColor="quartenary">
          <Heading size={2} caps lineHeight={1} textColor="primary">
            Componentes!
          </Heading>
          <Image src="https://www.thumb321.com.br/img-e07e14467e784b80"/>
          <List textColor="quartenary">
            <ListItem>Modularização e reaproveitamento de código</ListItem>
            <ListItem>Integração de código de terceiros</ListItem>
            <ListItem>Isolamento e desacoplação</ListItem>
          </List>
        </Slide>
        <Slide bgColor="quartenary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
            Gostou?
          </Heading>
          <Image src="https://media.giphy.com/media/W0DTCPYu9vrhu/giphy.gif"/>
        </Slide>
        <Slide bgColor="quartenary">
          <Heading size={2} caps lineHeight={1} textColor="tertiary">
            Vamos botar a mão na massa?
          </Heading>
          <Image src="https://media.giphy.com/media/c5eqVJN7oNLTq/giphy.gif"/>
        </Slide>
        <Slide bgColor="quartenary">
          <Heading size={2} caps lineHeight={1} textColor="tertiary">
            App de exemplo
          </Heading>
          <Text>insira aqui embed de codesandbox, app de faltas</Text>
        </Slide>
        <Slide bgColor="quartenary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
            Obrigado!
          </Heading>
          <Image src="https://media.giphy.com/media/5QRibvBQf7nMYceIcC/giphy.gif"/>
        </Slide>
      </Deck>
    );
  }
}
